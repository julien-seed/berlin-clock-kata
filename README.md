# README #

Implémentation de la feature 1 du projet "berlin-clock-kata" en java 8 et spring REST

http://agilekatas.co.uk/katas/BerlinClock-Kata


### Pré-requis ###

Maven
Tomcat
Java 8


### Compiler le projet ###

Dans un terminal, se placer dans le dossier "parent" et lancer la commande suivante:
mvn clean install


### Lancer le projet ###

Depuis eclipse avec un plugin tomcat ou en déployant le war généré lors de la compile.

war disponible après compile dans le dossier:
parent/bck-backend/ws-bck-serveur/target/


### Tester le service ###

Taper l'url suivante dans un navigateur:
http://localhost:8080/ws-bck-serveur/services/bck/getEntireBerlinClock?timeIn=23:45:58

Vous pouvez chnager le temps mais il doit être au format "hh:mm:ss" ou "hh:mm"
Le service renvoie le message "KO" si le format n'est pas correct.  