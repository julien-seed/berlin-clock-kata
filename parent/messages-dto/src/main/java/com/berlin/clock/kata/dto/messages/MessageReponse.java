package com.berlin.clock.kata.dto.messages;

public class MessageReponse {

	private String code;
	private String reponse;

	public MessageReponse( String code, String reponse ) {
		super();
		this.code = code;
		this.reponse = reponse;
	}

	public String getCode() {
		return code;
	}

	public void setCode( String code ) {
		this.code = code;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse( String reponse ) {
		this.reponse = reponse;
	}

}