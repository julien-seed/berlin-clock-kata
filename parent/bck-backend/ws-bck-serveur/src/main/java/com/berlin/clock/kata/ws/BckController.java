package com.berlin.clock.kata.ws;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.berlin.clock.kata.dto.messages.MessageReponse;
import com.berlin.clock.kata.services.BckService;
import com.berlin.clock.kata.services.exception.InvalidTimeException;

@RestController
@RequestMapping("/services/bck/")
public class BckController implements IBckController {

	private static final Logger logger = LogManager.getLogger( BckController.class );

	@Autowired
	private BckService bskService;

	//http://localhost:8080/ws-bck-serveur/services/bck/getEntireBerlinClock?timeIn=
	@RequestMapping(value = "/getEntireBerlinClock", method = RequestMethod.GET)
	@Override
	public MessageReponse getEntireBerlinClock( @RequestParam(value = "timeIn") String timeIn ) {
		String timeOut = null;
		try {
			timeOut = bskService.getEntireBerlinClock( timeIn );
		}
		catch ( InvalidTimeException e ) {
			logger.debug( new StringBuilder( "Invalid format for time string parameter  : " ).append( timeIn ).toString() );
			return new MessageReponse( e.getCode(), null );
		}
		return new MessageReponse( "OK", timeOut );
	}

}
