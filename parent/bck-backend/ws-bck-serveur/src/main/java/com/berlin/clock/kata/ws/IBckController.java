package com.berlin.clock.kata.ws;

import com.berlin.clock.kata.dto.messages.MessageReponse;

public interface IBckController {

	/**
	 * get enbtire berlin clock for time string parameter
	 * @param timeIn
	 * @return
	 */
	public MessageReponse getEntireBerlinClock( String timeIn );

}
