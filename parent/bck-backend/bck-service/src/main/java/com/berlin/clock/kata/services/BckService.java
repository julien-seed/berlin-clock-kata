package com.berlin.clock.kata.services;

import java.time.LocalTime;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.berlin.clock.kata.services.exception.InvalidTimeException;

@Service
public class BckService {

	private static final Logger logger = LogManager.getLogger( BckService.class );

	/**
	 * get entire berlin clock
	 * @param timeIn
	 * @return
	 * @throws InvalidTimeException
	 */
	public String getEntireBerlinClock( String timeIn ) throws InvalidTimeException {
		LocalTime time = null;
		try {
			time = LocalTime.parse( timeIn );
		}
		catch ( Exception e ) {
			logger.debug( new StringBuilder( "Time are not valid : " ).append( timeIn ).append( " - " ).append( e.getMessage() ) );
			throw new InvalidTimeException();
		}
		StringBuilder timeString = new StringBuilder();
		timeString.append( getSecondLamp( time.getSecond() ) );
		timeString.append( getFiveHoursRow( time.getHour() ) );
		timeString.append( getSingleHoursRow( time.getHour() ) );
		timeString.append( getFiveMinutesRow( time.getMinute() ) );
		timeString.append( getSingleMinutesRow( time.getMinute() ) );
		return timeString.toString();
	}

	/**
	 * get single minutes row
	 * @param minutes
	 * @return
	 */
	protected String getSingleMinutesRow( int minutes ) {
		return getOnOff( 4, minutes % 5, "Y" );
	}

	/**
	 * get five minutes row
	 * @param minutes
	 * @return
	 */
	protected String getFiveMinutesRow( int minutes ) {
		return getOnOff( 11, ( minutes - ( minutes % 5 ) ) / 5, "Y" ).replaceAll( "YYY", "YYR" );
	}

	/**
	 * get single hours row
	 * @param hours
	 * @return
	 */
	protected String getSingleHoursRow( int hours ) {
		return getOnOff( 4, hours % 5 );
	}

	/**
	 * get five hours row
	 * @param hours
	 * @return
	 */
	protected String getFiveHoursRow( int hours ) {
		return getOnOff( 4, ( hours - ( hours % 5 ) ) / 5 );
	}

	/**
	 * get second lamp
	 * @param seconds
	 * @return
	 */
	protected String getSecondLamp( int seconds ) {
		if ( seconds % 2 == 0 )
			return "Y";
		else
			return "O";
	}

	private String getOnOff( int lamps, int onSigns ) {
		return getOnOff( lamps, onSigns, "R" );
	}

	private String getOnOff( int lamps, int onSigns, String onSign ) {
		String out = "";
		for ( int i = 0; i < onSigns; i++ ) {
			out += onSign;
		}
		for ( int i = 0; i < ( lamps - onSigns ); i++ ) {
			out += "O";
		}
		return out;
	}

}
