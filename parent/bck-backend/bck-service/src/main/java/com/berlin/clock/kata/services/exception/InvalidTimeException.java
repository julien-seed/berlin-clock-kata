package com.berlin.clock.kata.services.exception;

/**
 * Exception in case of problem with format of the date
 */
public class InvalidTimeException extends Exception {

	private static final long serialVersionUID = -7887631997554598548L;

	/**
	 * error code
	 */
	private final String code = "KO";

	/**
	 * get error code
	 * @return
	 */
	public String getCode() {
		return code;
	}

}