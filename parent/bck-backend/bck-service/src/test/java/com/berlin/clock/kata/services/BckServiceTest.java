package com.berlin.clock.kata.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.berlin.clock.kata.services.exception.InvalidTimeException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class BckServiceTest {

	@Configuration
	static class ContextConfiguration {

		@Bean
		public BckService bckService() {
			BckService bckService = new BckService();
			return bckService;
		}
	}

	@Autowired
	private BckService bckService;

	@Test
	public void getSingleMinutesRowTest(  ) {
		assertEquals(bckService.getSingleMinutesRow(0), "OOOO");

		assertEquals(bckService.getSingleMinutesRow(59), "YYYY");

		assertEquals(bckService.getSingleMinutesRow(32), "YYOO");

		assertEquals(bckService.getSingleMinutesRow(34), "YYYY");

		assertEquals(bckService.getSingleMinutesRow(35), "OOOO");

	}

	@Test
	public void getFiveMinutesRowTest( ){
		assertEquals(bckService.getFiveMinutesRow(0), "OOOOOOOOOOO");

		assertEquals(bckService.getFiveMinutesRow(59), "YYRYYRYYRYY");

		assertEquals(bckService.getFiveMinutesRow(4), "OOOOOOOOOOO");

		assertEquals(bckService.getFiveMinutesRow(23), "YYRYOOOOOOO");

		assertEquals(bckService.getFiveMinutesRow(35), "YYRYYRYOOOO");
	}

	@Test
	public void getSingleHoursRowTest(){
		assertEquals(bckService.getSingleHoursRow(0), "OOOO");

		assertEquals(bckService.getSingleHoursRow(23), "RRRO");

		assertEquals(bckService.getSingleHoursRow(2), "RROO");

		assertEquals(bckService.getSingleHoursRow(8), "RRRO");

		assertEquals(bckService.getSingleHoursRow(14), "RRRR");
	}

	@Test
	public void getFiveHoursRowTest(){
		assertEquals(bckService.getFiveHoursRow(0), "OOOO");

		assertEquals(bckService.getFiveHoursRow(23), "RRRR");

		assertEquals(bckService.getFiveHoursRow(2), "OOOO");

		assertEquals(bckService.getFiveHoursRow(8), "ROOO");

		assertEquals(bckService.getFiveHoursRow(16), "RRRO");
	}

	@Test
	public void getSecondLampTest(){
		assertEquals(bckService.getSecondLamp(0), "Y");

		assertEquals(bckService.getSecondLamp(59), "O");
	}

	@Test
	public void getEntireBerlinClockTest() throws InvalidTimeException{
		assertEquals(bckService.getEntireBerlinClock("00:00:00"), "YOOOOOOOOOOOOOOOOOOOOOOO");

		assertEquals(bckService.getEntireBerlinClock("23:59:59"), "ORRRRRRROYYRYYRYYRYYYYYY");

		assertEquals(bckService.getEntireBerlinClock("16:50:06"), "YRRROROOOYYRYYRYYRYOOOOO");

		assertEquals(bckService.getEntireBerlinClock("11:37:01"), "ORROOROOOYYRYYRYOOOOYYOO");
	}

	@Test(expected=InvalidTimeException.class)
	public void getEntireBerlinClockInvalidFormatTest() throws InvalidTimeException{
		bckService.getEntireBerlinClock("ne fonctionne pas");
	}

}
